An Improved Multi-path AODV Protocols for real-time video transport in Ad-hoc network
========
Nama: Fauzan Abid Ramadhan
NRP: 5115100156
--------
Penjelasan singkat:<br>
a. Mengimprovisasi Single Route (AODV) menjadi multipath (AOMDV)
Dalam sekali route discovery AOMDV dapat membangun lebih dari satu path dari source ke destination. Dapat diartikan sebagai rute cadangan. Maka dari itu, didirikan reverse path yang tidak mengirim pesan RREQ saat broadcasting disaat intermediate nodes menerima pesan RREQ yang sama.
Sebagai contoh, disaat RREQ dikirim dari node 1, pada node ke 6 akan menerima pesan RREQ yang sama sebanyak 3 kali. Akan tetapi, node 6 hanya akan menerima RREQ yang pertama kali sampai. Saat menerima RREQ yang sama, dia tidak akan melakukan apa-apa selain membuat reverse path ke node 1.<br> 
![alt text](/gambar/55.png "AOMDV")<br>
b. Mengimprovisasi dalam pemilihan Path
Pemilihan path berdasarkan jumlah energi terbesar dan delay terkecil untuk dijadikan rute utama. Jalur yang lain (berdasarkan urutan ketentuan) akan dijadikan sebagai rute sekunder, tersier, dsb.
